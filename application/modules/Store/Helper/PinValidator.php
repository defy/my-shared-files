<?php

namespace Store\Helper;

/**
 * Description of PinValidator
 *
 * @author GeN
 */
class PinValidator
{

	public function __construct($options)
	{
		if (isset($options['validator']) && $options['validator'] instanceof \Core\Form\Validator) {
			$validator = $options['validator'];
		} else {
			return $this;
		}

		$item = false;
		if($options['parent'] instanceof \Store\CreateItemController)
			$item = true;
		if($options['parent'] instanceof \Store\EditController)
			$item = true;
		
		if(!$item)
			return $this;
		
		$translate = new \Translate\Locale('Front\\' . __NAMESPACE__, \Core\Base\Action::getModule('Language')->getLanguageId());

		$check = \Seller\Helper\Subscription::enableAddItem(\User\User::getUserData()->id, isset($options['pin']->id)?$options['pin']->id:0);
		if (!$check) {
			$validator->addNumber('atleast-one2', array(
				'error_text' => $translate->_('You must be a seller!')
			));
			return $this;
		}
		
		$request = \Core\Http\Request::getInstance();
		if((int)$request->getPost('digital_product'))
			return $this;
		if((int)$request->getPost('auction'))
			return $this;
		
		
		$sell = $request->getPost('sell');
		$data = $request->getPost('pinquantity');
		if ($sell && isset($data['price'])) {
			// Check title
			$validator->addText('title', array(
				'min' => 3,
				'error_text_min' => $translate->_('Title must contain no less than %d characters')
			));
			// check for only one empty option
			$options = $request->getPost('pinquantity[option]');
			$options = is_array($options) ? $options : array();
			$emptyOptions = count(array_filter($options, array($this, 'checkEmpty')));
			if ($emptyOptions > 1) {
				$validator->addNumber('only-one', array(
					'error_text' => $translate->_('You can add only one empty option')
				));
			}
			// check for unique options
			if (count($data['option']) != count(array_unique($data['option']))) {
				$validator->addNumber('unique', array(
					'error_text' => $translate->_('You can add only unique options')
				));
			}
			// check quantity and price
			foreach ($data['price'] as $key => $val) {
				$validator->addNumber('quantity' . $key, array(
					'min' => 1,
					'step' => 1,
					'custom-value' => $request->getPost('pinquantity[quantity][' . $key . ']'),
					'error_text' => $translate->_('Please enter a valid quantity')
				));
				$validator->addNumber('price' . $key, array(
					'min' => 0.01,
					'custom-value' => $val,
					'error_text' => $translate->_('Please enter a valid price')
				));
			}
		} elseif ($sell) {
			$validator->addNumber('atleast-one', array(
				'error_text' => $translate->_('You must add at least one record for quantity and price')
			));
		}
		
		if($sell && !\Store\Settings::getCurrencyCode()) {
			$validator->addNumber('atleast-one2', array(
					'error_text' => $translate->_('You must select your default currency in shop settings!')
			));
		}
		
	}

	private function checkEmpty($param)
	{
		$param = trim($param);
		return empty($param);
	}

}